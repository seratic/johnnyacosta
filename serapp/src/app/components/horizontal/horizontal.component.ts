import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';

@Component({
    selector     : 'scroll-horizontal',
    templateUrl  : './horizontal.component.html',
    styleUrls    : ['./horizontal.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ScrollHorizontalComponent implements AfterViewInit {
    width = window.innerWidth;
    idItems = uuidv4();
    idL = uuidv4();
    idR = uuidv4();


    constructor() {
        console.log(this.idL)
    }

    ngAfterViewInit(): void {
        window.addEventListener('resize', (e) => this.width = window.innerWidth);
        const el = document.getElementById(this.idItems);
        el.addEventListener('scroll', (e) => this.showControls(el));
    }

    handleScrollBar = (direction: string) => {
        console.log("handel",direction)
        const el = document.getElementById(this.idItems);
        console.log(el);
        
        switch (direction) {
            case 'left':
                el.scrollLeft -= 10;
                break;
            case 'right':
                el.scrollLeft += 10;
                break;
        }
    }

    showControls(el: HTMLElement) {

        if (el.scrollLeft <= 3) {
            document.getElementById(this.idL).style.visibility = 'initial';
        } else {
            document.getElementById(this.idL).style.visibility = 'initial';
        }

        if (el.scrollLeft >= el.scrollWidth-this.width) {
            document.getElementById(this.idR).style.visibility = 'unitial';
        } else {
            document.getElementById(this.idR).style.visibility = 'initial';
        }
    }
}
