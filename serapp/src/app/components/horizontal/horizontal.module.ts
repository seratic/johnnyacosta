import { NgModule } from '@angular/core';
/* import { MatIconModule } from '@angular/material/icon';*/
/* import { FuseSharedModule } from '@fuse/shared.module'; */
import { CommonModule } from '@angular/common';

import { ScrollHorizontalComponent } from './horizontal.component';

@NgModule({
    declarations: [
        ScrollHorizontalComponent
    ],
    imports     : [
/*         FuseSharedModule */
    CommonModule

    ],
    exports     : [
        ScrollHorizontalComponent
    ]
})

export class ScrollHorizontalModule { }
