import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ControladorService } from 'src/app/servicios/controlador.service';

@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.scss']
})
export class AddProductsComponent implements OnInit {
  @ViewChild('inputFile',{static:false}) file:any;
  @ViewChild('dataImage',{static:false}) dataImage:any;
  @ViewChild('imageFirm',{static:false}) imageFirm:any;
  formulario:FormGroup;
  photo:any;

  constructor(
    private _builder: FormBuilder,
    private controller:ControladorService) {
      this.formulario = _builder.group({
        nombre:["",Validators.required],
        descripcion:["",Validators.required],
        precio:["",Validators.required],
        imagen:["",Validators.required],
        dataImage:[""],
        dto:["",Validators.required]
      })
     }

  ngOnInit(): void {
  }


  showImagen($event: any): void {
    let photo = $event.target.files[0];

    this.getBase64(photo).then(
      data => this.photo = data
    );

    console.log(this.photo);
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }


  send(values){
    this.controller.post("urlServe",values)
  }



}
