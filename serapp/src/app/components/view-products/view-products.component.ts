import { Component, OnInit } from '@angular/core';
import { ControladorService } from 'src/app/servicios/controlador.service';

@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.scss']
})
export class ViewProductsComponent implements OnInit {

  products:any=[];
  productsRandon:any =[];

  constructor(private controller:ControladorService) {
    this.products = this.controller.get("url");
    this.fillRamdon();
    //console.log("LISTA RADOM",this.productsRandon);
    /* this.controller.get("url")
    .subscribe(json =>{ this.products = json}) */
   }

  ngOnInit(): void {
    
  }

  fillRamdon(){
    while (this.productsRandon.length<4){
      let num = Math.random() * this.products.length;
      num = Math.floor(num);
      this.productsRandon.push(this.products[num]);
    }
    
  }

}
