import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input('dato') dato:any;
  @Input('id') id:any;
  constructor() {
   }

  ngOnInit(): void {
    console.log("input presentacion",this.dato);
  }

  go(){
    window.location.href = `detail:${this.id}`;
  }

}
