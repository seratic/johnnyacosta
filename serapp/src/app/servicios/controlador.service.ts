import { Injectable } from '@angular/core';
import { HttpClient,HttpParams, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ControladorService {

  constructor(private http:HttpClient) { }

  public get(url:string,data?:any){
    let productos = [
      {nombre:"cafe1",descricion:"descripcion cafe 1",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe2",descricion:"descripcion cafe 2",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe3",descricion:"descripcion cafe 3",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe4",descricion:"descripcion cafe 4",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe5",descricion:"descripcion cafe 5",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe6",descricion:"descripcion cafe 6",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe7",descricion:"descripcion cafe 7",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe8",descricion:"descripcion cafe 8",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe9",descricion:"descripcion cafe 9",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe10",descricion:"descripcion cafe 10",precio:"500",imagen:"url",descuento:"10"},
      {nombre:"cafe11",descricion:"descripcion cafe 11",precio:"500",imagen:"url",descuento:"10"},
    ]
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    //return this.http.post(url,data,{headers});
    return productos;
  }

  public post(url:string, data:any){
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    console.log(data)
    debugger;
    return this.http.post(url,data,{headers});
  }
}
