import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewProductsComponent } from './components/view-products/view-products.component';
import { AddProductsComponent } from './components/add-products/add-products.component';
import { NavComponent } from './components/nav/nav.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ScrollHorizontalModule } from './components/horizontal/horizontal.module';

@NgModule({
  declarations: [
    AppComponent,
    ViewProductsComponent,
    AddProductsComponent,
    NavComponent,
    ProductDetailComponent,
    ProductCardComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollHorizontalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
